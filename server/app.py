from flask import Flask, render_template
from flask_pymongo import PyMongo
from flask_restful import Resource, Api
from flask_jwt import JWT, jwt_required, current_identity

from config import BaseConfig as current_config

from resources.arbitrage_task import ArbitrageTaskAPI
from resources.user import UsersAPI

import models

# Set up a Flask app and load configuration
app = Flask(__name__)
app.config.from_object(current_config)

# Set up mongodb connection
mongo = PyMongo(app)

# Set up the API endpoints
api = Api(app)
api.add_resource(UsersAPI, '/user')
api.add_resource(ArbitrageTaskAPI, '/arbitrage_task')

# Set up authentication - json web token
jwt = JWT(app, models.verify, models.identity)

if __name__ == '__main__':
	app.run(host=current_config.HOST,
            port=current_config.PORT,
            threaded=current_config.THREADED)