class User(object):
    def __init__(self, id, password):
        self.id = id
        self.name = "User-" + str(id)
        self.password = password
        
    def __repr__(self):
        return "%d/%s/%s" % (self.id, self.name, self.password)

USER_DATA = {
    "daniel": "123"
}

def verify(username, password):
    if not (username and password):
        return False
    if USER_DATA.get(username) == password:
        return User(id=123,password=password)


def identity(payload):
    user_id = payload['identity']
    return {"user_id": user_id}
