from flask_restful import Resource
from flask_jwt import jwt_required

class ArbitrageTaskAPI(Resource):
    
    @jwt_required()
    def get(self):
        """
            Returns a list of all the tasks for the currently authenticated user
        """
        return {
            'task_id': '7',
            'user_id': '9',
            'name': 'My awesome task',
            'markets': ['kraken', 'bitstamp']
        }

    def post(self):
        """
           Creates a new Arbitrage task for the current user
        """
        pass