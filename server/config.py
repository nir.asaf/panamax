class BaseConfig(object):
    SECRET_KEY = "MySuperSecureKey"
    DEBUG = True
    HOST = "0.0.0.0"
    PORT = 8080
    THREADED = True
    MONGO_DBNAME = "panamax"
    MONGO_URI = "mongodb://localhost:27017/panamax"

class TestingConfig(object):
    """Development configuration."""
    TESTING = True
    DEBUG = True
